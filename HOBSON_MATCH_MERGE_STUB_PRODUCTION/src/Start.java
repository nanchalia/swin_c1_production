import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.util.ArrayList;

import org.apache.axis2.AxisFault;

import com.rightnow.ws.objects.Answer;
import com.rightnow.ws.wsdl.RequestErrorFault;
import com.rightnow.ws.wsdl.RequestErrorFaultDetail;
import com.rightnow.ws.wsdl.ServerErrorFault;
import com.rightnow.ws.wsdl.UnexpectedErrorFault;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class Start {
	
	final static String inputFile = "D:\\PROJECTS\\SWINBURNE\\SWIN_HOBSON_CONTACT_IMPORTER_TOOL\\DATALOAD\\UGTF_20171001_SPLIT\\UGTF_01.csv";
	//final static String highestQualification = "D:\\PROJECTS\\SWINBURNE\\SWIN_HOBSON_CONTACT_IMPORTER_TOOL\\LOOKUP\\HighestQualification.csv";
	//final static String bestDescribes = "D:\\PROJECTS\\SWINBURNE\\SWIN_HOBSON_CONTACT_IMPORTER_TOOL\\LOOKUP\\BestDescribes.csv";
	//final static String country = "D:\\PROJECTS\\SWINBURNE\\SWIN_HOBSON_CONTACT_IMPORTER_TOOL\\LOOKUP\\Countries.csv";
	//final static String areaOfStudy = "D:\\PROJECTS\\SWINBURNE\\SWIN_HOBSON_CONTACT_IMPORTER_TOOL\\LOOKUP\\AreaOfStudy.csv";
	//final static String contactChannel = "D:\\PROJECTS\\SWINBURNE\\SWIN_HOBSON_CONTACT_IMPORTER_TOOL\\LOOKUP\\ContactChannel.csv";
	final static Logger log = Logger.getLogger("FileRead");
	
	public static void main(String[] args) throws AxisFault, RequestErrorFaultDetail {
		// Initialize logger
		PropertyConfigurator.configure("Log4j.properties");
		RecordCollector inputData = new RecordCollector();
		RecordCollector originalC1Contact = new RecordCollector();
		RecordCollector newContact = new RecordCollector();
		RecordCollector finalImportData = new RecordCollector();
		RNWebservice rnWebservice = new RNWebservice();
		log.info("Processing started...");
		boolean fileReadComplete = (inputData.readInputFile(inputFile));
						
		//START PROCESSING
		if(fileReadComplete){
			log.info("Input file read complete");
			log.info("Querying and compare records with C1");
			//for(int i=0;i<inputData.getRecordCount();i++){
			for(int i=0; i<10; i++){
				//Backup contact if present in C1
				log.info("processing record#"+i+"/ hobsons crm id: " + inputData.getRecord(i).getHobsons_CRM_ID());
				Record temp = null;
				Record c1Contact = rnWebservice.RNCheckRecord(inputData.getRecord(i));
				if(c1Contact !=null){
					//take backup 
					originalC1Contact.addRecord(c1Contact);
					temp = inputData.compareRecords(i, c1Contact);
				}
				else{
					newContact.addRecord(inputData.getRecord(i));
					temp = inputData.getRecord(i);
				}
				
				//Compare C1 record with input record
				//temp = inputData.compareRecords(i);
				if(temp != null){
					finalImportData.addRecord(temp);
				}
			}
		}
		if(finalImportData != null){
			log.info("Preparing final dataload file");
			//for(int i=0; i<finalImportData.getRecordCount(); i++){
				//finalImportData.printRecord(i);
			//}
			finalImportData.createCSV(1);
			originalC1Contact.createCSV(2);
			newContact.createCSV(3);
		}
	}

}
