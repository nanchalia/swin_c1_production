import org.apache.axis2.AxisFault;
import org.apache.axis2.Constants;

import com.rightnow.ws.wsdl.RightNowSyncService;
import com.rightnow.ws.wsdl.RightNowSyncServiceStub;

//LIBRARIES REQUIRED FOR SECURITY HEADER
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.impl.OMNamespaceImpl;
import org.apache.axiom.om.impl.llom.OMElementImpl;
import org.apache.axiom.soap.impl.llom.soap11.SOAP11Factory;
import org.apache.ws.security.WSConstants;
import org.apache.axis2.client.ServiceClient;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

//LIBRARIES FOR OBJECT

import com.rightnow.ws.base.ActionEnum;
import com.rightnow.ws.base.ID;
import com.rightnow.ws.base.NamedID;
import com.rightnow.ws.base.RNObject;
import com.rightnow.ws.generic.DataTypeEnum;
import com.rightnow.ws.generic.DataValue;
import com.rightnow.ws.generic.GenericField;
import com.rightnow.ws.generic.GenericObject;
import com.rightnow.ws.generic.RNObjectType;
import com.rightnow.ws.messages.ClientInfoHeader;
import com.rightnow.ws.messages.CreateProcessingOptions;
import com.rightnow.ws.messages.GetProcessingOptions;
import com.rightnow.ws.messages.QueryResultData;
import com.rightnow.ws.messages.RNObjectsResult;
import com.rightnow.ws.messages.UpdateProcessingOptions;
import com.rightnow.ws.messages.UpdateResponseMsg;
import com.rightnow.ws.objects.*;

import com.rightnow.ws.wsdl.RequestErrorFault;
import com.rightnow.ws.wsdl.RequestErrorFaultDetail;
import com.rightnow.ws.wsdl.ServerErrorFault;
import com.rightnow.ws.wsdl.UnexpectedErrorFault;
import java.rmi.RemoteException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;

public class RNWebservice {
	RightNowSyncService _service;
	final static Logger log = Logger.getLogger("RNWebservice");
	final static String highestQualification = "D:\\PROJECTS\\SWINBURNE\\SWIN_HOBSON_CONTACT_IMPORTER_TOOL\\LOOKUP\\HighestQualification.csv";
	final static String bestDescribes = "D:\\PROJECTS\\SWINBURNE\\SWIN_HOBSON_CONTACT_IMPORTER_TOOL\\LOOKUP\\BestDescribes.csv";
	final static String country = "D:\\PROJECTS\\SWINBURNE\\SWIN_HOBSON_CONTACT_IMPORTER_TOOL\\LOOKUP\\Countries.csv";
	final static String areaOfStudy = "D:\\PROJECTS\\SWINBURNE\\SWIN_HOBSON_CONTACT_IMPORTER_TOOL\\LOOKUP\\AreaOfStudy.csv";
	final static String contactChannel = "D:\\PROJECTS\\SWINBURNE\\SWIN_HOBSON_CONTACT_IMPORTER_TOOL\\LOOKUP\\ContactChannel.csv";
	private RecordCollector inputData = new RecordCollector();
	private RecordCollector finalImportData = new RecordCollector();
	private RecordCollector countryLookup = new RecordCollector();
	private RecordCollector highestQualificationLookup = new RecordCollector();
	private RecordCollector areaOfStudyLookup = new RecordCollector();
	private RecordCollector bestDescribesLookup = new RecordCollector();
	private RecordCollector contactChannelLookup = new RecordCollector();
	
	boolean countryReadComplete =false;
	boolean hqReadComplete = false;
	boolean bdReadComplete = false;
	boolean areaOfStudyReadComplete = false;
	boolean contactChannelReadComplete = false;
	
	public RNWebservice() throws AxisFault{
		countryReadComplete = countryLookup.readLookupFile(country);
		hqReadComplete = highestQualificationLookup.readLookupFile(highestQualification);
		bdReadComplete = bestDescribesLookup.readLookupFile(bestDescribes);
		areaOfStudyReadComplete = areaOfStudyLookup.readLookupFile(areaOfStudy);
		contactChannelReadComplete = contactChannelLookup.readLookupFile(contactChannel);
		_service = new RightNowSyncServiceStub();
		ServiceClient serviceClient = ((org.apache.axis2.client.Stub)_service)._getServiceClient();
		serviceClient.addHeader(createSecurityHeader("nanchalia", "Nikunj@customer1"));
		
		
	}
	
	//public static void main(String[] args) throws RequestErrorFaultDetail {
	public Record RNCheckRecord(Record inputRecord) throws RequestErrorFaultDetail{
		PropertyConfigurator.configure("Log4j.properties");
		
		//String emailAddr = "'nanchalia@swin.edu.au'";
		String emailAddr = "'"+inputRecord.getEmail()+"'";
		String queryString = "Select Contact from Contact where Contact.Emails.EmailList.Address = " + emailAddr;
		Record c1Record = null;
		//log.info("Log file loaded: ");
		try
		{
			c1Record = this.invokeRNWebservice(queryString);
			if((c1Record == null)&& (inputRecord.getEmail2() != null)){
				emailAddr = "'" + inputRecord.getEmail2() + ",";
				c1Record = this.invokeRNWebservice(queryString);
			}
			/*
			RNWebservice SWINTestStub = new RNWebservice();
			QueryResultData[] queryObjects = SWINTestStub.rnQueryObject(queryString);
			RNObjectsResult rnObjectsResult = queryObjects[0].getRNObjectsResult();
			RNObject[] rnObjects = rnObjectsResult.getRNObjects();
			if(rnObjects != null){
				for(RNObject obj: rnObjects){
					Contact contact = (Contact)obj;
					c1Record = SWINTestStub.getContact(contact.getID().getId());
					
				}
			}
			else{
				System.out.println("No Record found in C1");
				
			}
			*/
		}
		catch(AxisFault e)
		{
			System.out.println("Webservice Error: "+e.getMessage());
		}
		
		//System.out.println(a);
		return c1Record;
	}
	
	private Record invokeRNWebservice(String queryString) throws RequestErrorFaultDetail, AxisFault{
		Record c1Record = null;
		RNWebservice SWINTestStub = new RNWebservice();
		QueryResultData[] queryObjects = SWINTestStub.rnQueryObject(queryString);
		RNObjectsResult rnObjectsResult = queryObjects[0].getRNObjectsResult();
		RNObject[] rnObjects = rnObjectsResult.getRNObjects();
		if(rnObjects != null){
			for(RNObject obj: rnObjects){
				Contact contact = (Contact)obj;
				c1Record = SWINTestStub.getContact(contact.getID().getId());
				
			}
		}
		else{
			System.out.println("No Record found in C1");
			return null;
		}
	return c1Record;	
	}
	//SETUP REQUEST HEADER
	private static OMElement createSecurityHeader(String username, String password){
		OMFactory factory = new SOAP11Factory();
		OMNamespace wsseNS = factory.createOMNamespace(WSConstants.WSSE_NS, WSConstants.WSSE_PREFIX);
		
		OMElement usernameTokenElement = factory.createOMElement(WSConstants.USERNAME_TOKEN_LN, wsseNS);
		
		//USERNAME
		OMElement usernameElement = factory.createOMElement(WSConstants.USERNAME_LN, wsseNS);
		usernameElement.setText(username);
		usernameTokenElement.addChild(usernameElement);
		
		//PASSWORD
		OMElement passwordElement = factory.createOMElement(WSConstants.PASSWORD_LN, wsseNS);
		passwordElement.setText(password);
		passwordElement.addAttribute(WSConstants.PASSWORD_TYPE_ATTR, WSConstants.PASSWORD_TEXT, null);
		usernameTokenElement.addChild(passwordElement);
		
		//Security Header
		OMElement securityHeader = factory.createOMElement("Security", wsseNS);
		securityHeader.addAttribute("mustUnderstand", "1", null);
		securityHeader.addChild(usernameTokenElement);
		
		return securityHeader;
	}
	
	//QUERY RN OBJECT BY EMAIL
	public QueryResultData[] rnQueryObject(String queryString) throws RequestErrorFaultDetail{
		ClientInfoHeader clientInfoHeader = new ClientInfoHeader();
		clientInfoHeader.setAppID("Basic Object Query");
		//String emailAddr = "'nanchalia@swin.edu.au'";
		//String queryString = "Select Contact from Contact where Contact.Emails.EmailList.Address ="+emailAddr;
		QueryResultData[] queryObjects = null;
		
		Contact contactTemplate = new Contact();
		contactTemplate.setEmails(new EmailList());
		
		
		RNObject[] objectTemplates = new RNObject[] {contactTemplate};
		try{
				queryObjects = _service.queryObjects(queryString, objectTemplates, 1000, clientInfoHeader);
				/*
				RNObjectsResult rnObjectsResult = queryObjects[0].getRNObjectsResult();
				RNObject[] rnObjects = rnObjectsResult.getRNObjects();
				if(rnObjects != null){
					for(RNObject obj: rnObjects){
						Contact contact = (Contact)obj;
						getContact(contact.getID().getId());
						
					}
				}
				else{
					System.out.println("No Record found");
				}*/
		}
		catch (RemoteException e)
        {
                System.out.println("Remote exception encountered: " + e.getMessage());
        }
        catch (ServerErrorFault e)
        {
                System.out.println("Server error exception encountered: " + e.getFaultMessage().getServerErrorFault().getExceptionMessage());
        }
        catch (RequestErrorFault e)
        {
                System.out.println("Request error exception encountered: " + e.getFaultMessage().getRequestErrorFault().getExceptionMessage());
        }
        catch (UnexpectedErrorFault e)
        {
                System.out.println("Unexpected error exception encountered:" + e.getFaultMessage().getUnexpectedErrorFault().getExceptionMessage());    
        }
		return queryObjects;
	}
	
	//PERFORM BASIC GET TO GET CONTACT DETAILS
	public Record getContact(long conID) throws RequestErrorFaultDetail{
		Contact contact = new Contact();
		//TO DO: Convert to array incase search results in multiple records
		Record queryRecord = null;
		
		//Create and Set ID property
		ID contactID = new ID();
		contactID.setId(conID);
		
		//SET ID OF PRIMARY OBJECT
		contact.setID(contactID);
		
		//GET CHILD OBJECTS FOR CONTACT - EMAIL AND CUSTOM FIELDS
		EmailList emailList = new EmailList();
		contact.setEmails(emailList);
		
		PhoneList phoneList = new PhoneList();
		contact.setPhones(phoneList);
		GenericObject genericObject = new GenericObject();
		contact.setCustomFields(genericObject);
		
	    //BUILD THE RN OBJECT ARRAY
		RNObject[] objects = new RNObject[]{contact};
		
		//BUILD AND SET PROCESSIN OPTION
		GetProcessingOptions options = new GetProcessingOptions();
		options.setFetchAllNames(false);
		
		try{
			ClientInfoHeader clientInfoHeader = new ClientInfoHeader();
			clientInfoHeader.setAppID("Basic Get");
			
			RNObjectsResult getResults = _service.get(objects, options, clientInfoHeader);
			RNObject[] rnObjects = getResults.getRNObjects();
			if(rnObjects != null){
				for(RNObject rnObject : rnObjects){
					contact = (Contact)rnObject;
					queryRecord = getContactDetails(contact);
				}
			}
			//if(queryRecord != null){
				//System.out.println("Hobsons CRMID: " + queryRecord.getHobsons_CRM_ID());
				//System.out.println("First Name: " + queryRecord.getFirstName());
				//System.out.println("LastName: " + queryRecord.getLastName());
				//System.out.println("Email: " + queryRecord.getEmail());
				//System.out.println("Email2: " + queryRecord.getEmail2());
				//System.out.println("Date of Birth: "+ queryRecord.getDOB());
				//System.out.println("Subscribed email: "+ queryRecord.getSubscription_Email());
				//System.out.println("Subscribed phone: "+ queryRecord.getSubscription_Phone());
				//System.out.println("Subscribed SMS:"+ queryRecord.getSubscription_SMS());
				//System.out.println("International: "+ queryRecord.getInternational());
				//System.out.println("Country of Residence: " + queryRecord.getCountryOfResidence());
				//System.out.println("CorrespondenceMobile: " + queryRecord.getCorrespondenceMobile());
				//System.out.println("CorrespondenceAddressPostCode: " + queryRecord.getCorrespondenceAddressPostcode());
				//System.out.println("Course: "+ queryRecord.getCourse());
				//System.out.println("Area of Study: " + queryRecord.getCourseAreaOfStudy());
				//System.out.println("Best Describes you: " + queryRecord.getBestDescribesYou());
				//System.out.println("Highest Qualification: " + queryRecord.getHighestQualification());
				//System.out.println("Contact Channel: "+ queryRecord.getContactChannel());
			//}
		}
		catch (RemoteException e)
        {
                System.out.println("Remote exception encountered: " + e.getMessage());
        }
        catch (ServerErrorFault e)
        {
                System.out.println("Server error exception encountered: " + e.getFaultMessage().getServerErrorFault().getExceptionMessage());
        }
        catch (RequestErrorFault e)
        {
                System.out.println("Request error exception encountered: " + e.getFaultMessage().getRequestErrorFault().getExceptionMessage());
        }
        catch (UnexpectedErrorFault e)
        {
                System.out.println("Unexpected error exception encountered:" + e.getFaultMessage().getUnexpectedErrorFault().getExceptionMessage());    
        }
		return queryRecord;
	}
	
	//DISPLAY CONTACT DETAILS
	private Record getContactDetails(Contact contact){
		Record record = new Record();
		if(contact.getName()!=null){
			record.setFirstName((contact.getName().getFirst()!= null)? contact.getName().getFirst(): "");
			record.setLastName((contact.getName().getLast()!= null)? contact.getName().getLast(): "");
		}
		//record.setCountryOfResidence("'"+contact.getAddress().getCountry().getID().getId()+"'");
		//GET COUNTRY OF RESIDENCE
		if(contact.getAddress()!= null){
			if((contact.getAddress().getCountry() != null)){
				record.setCountryOfResidence(this.countryLookup.lookupValue(contact.getAddress().getCountry().getID().getId()));
			}
			if(contact.getAddress().getPostalCode()!= null){
				record.setCorrespondenceAddressPostcode(contact.getAddress().getPostalCode());
			}
		}
		//GET phone
		if(contact.getPhones()!= null){
			Phone[] phones = contact.getPhones().getPhoneList();
			 	
			if(phones!=null){
				for(Phone ph : phones){
					//Get Mobile only
					if(ph.getPhoneType().getID().getId() == 1){
						record.setCorrespondenceMobile(ph.getNumber());
					}
				}
			}
		}
			
		
		//Get Email
		EmailList e = contact.getEmails();
		if(e!=null){
			Email[] eList = e.getEmailList();
			if(eList != null){
				//for(Email list: eList){
					//System.out.println("Contact Email: "+list.getAddress());
				//}
				record.setEmail(eList[0].getAddress());
				//record.setEmail2(eList[1].getAddress());
			}
		}
		
		//Get Custom Fields
		GenericField[] genericFields = contact.getCustomFields().getGenericFields();
		for(GenericField genericField : genericFields){
			GenericObject customField = genericField.getDataValue().getObjectValue();
			GenericField[] customFieldList = customField.getGenericFields();
			
			if(customFieldList != null){
				//System.out.println("List not null");
				for(GenericField gf: customFieldList){
					//System.out.println(gf.getName() +"/" + gf.getDataType());
					if(gf.getName().compareTo("DOB")==0){
						record.setDOB(getCustomFieldValue(gf));
						//System.out.println(record.getDOB());
					}
					if(gf.getName().compareTo("subscribed_email")==0){
						record.setSubscription_Email(getCustomFieldValue(gf));
					}
					if(gf.getName().compareTo("subscribed_phone")==0){
						record.setSubscription_Phone(getCustomFieldValue(gf));
					}
					if(gf.getName().compareTo("subscribed_sms")==0){
						record.setSubscription_SMS(getCustomFieldValue(gf));
					}
					if(gf.getName().compareTo("international")==0){
						record.setInternational(getCustomFieldValue(gf));
					}
					if(gf.getName().compareTo("course_name")==0){
						record.setCourse(getCustomFieldValue(gf));
					}
					if(gf.getName().compareTo("area_of_study")==0){
						record.setCourseAreaOfStudy(getCustomFieldValue(gf));
					}
					if(gf.getName().compareTo("best_describes_you")==0){
						record.setBestDescribesYou(getCustomFieldValue(gf));
					}
					if(gf.getName().compareTo("highest_qualification")==0){
						record.setHighestQualification(getCustomFieldValue(gf));
					}
					if(gf.getName().compareTo("contact_channel")==0){
						record.setContactChannel(getCustomFieldValue(gf));
					}
				}
			}
			
		}
		return record;
	}
	
	//Gets Value of Custom Field
	private String getCustomFieldValue(GenericField gf){
		String value = null;
		DataValue dataValue = gf.getDataValue();
		if(dataValue !=null){
			if(dataValue.isNamedIDValueSpecified()){
				//value =  "'" + dataValue.getNamedIDValue().getID().getId()+ "'";
				if(this.areaOfStudyLookup.lookupValue(dataValue.getNamedIDValue().getID().getId()) != null){
					value = this.areaOfStudyLookup.lookupValue(dataValue.getNamedIDValue().getID().getId());
				}
				if(this.bestDescribesLookup.lookupValue(dataValue.getNamedIDValue().getID().getId()) != null){
					value = this.bestDescribesLookup.lookupValue(dataValue.getNamedIDValue().getID().getId());
				}
				if(this.highestQualificationLookup.lookupValue(dataValue.getNamedIDValue().getID().getId()) != null){
					value = this.highestQualificationLookup.lookupValue(dataValue.getNamedIDValue().getID().getId());
				}
				
				//International
				if(dataValue.getNamedIDValue().getID().getId() == 518){
					value = "Domestic";
				}
				if(dataValue.getNamedIDValue().getID().getId() == 519){
					value = "International";
				}
			}
			
			if(dataValue.isDateValueSpecified()){
				
				DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
				//TimeZone utc = TimeZone.getTimeZone("Autralia/Sydney");
				//df.setTimeZone(utc);
				value = df.format(dataValue.getDateValue());
				//value = dataValue.getDateValue();
				
			}
			if(dataValue.isBooleanValueSpecified()){
				//value = (String)gf.getName()+":"+dataValue.getBooleanValue();
				if(dataValue.getBooleanValue()){
					value = "Yes";
				}
				else{
					value = "No";
				}
				
			}
			
		}
		return value;
	}
	
	/* WORKING CODE - RESTRUCTURED
	public void getContact(long conID) throws RequestErrorFaultDetail{
				
		Boolean isCustomFieldsSpecified = false;
		Contact contact = new Contact();
		
		//Create and Set ID property
		ID contactID = new ID();
		contactID.setId(conID);
		
		//SET ID OF PRIMARY OBJECT
		contact.setID(contactID);
		
		//GET CHILD OBJECTS FOR CONTACT - EMAIL AND CUSTOM FIELDS
		EmailList emailList = new EmailList();
		contact.setEmails(emailList);
		
		GenericObject genericObject = new GenericObject();
		contact.setCustomFields(genericObject);
		
	    //BUILD THE RN OBJECT ARRAY
		RNObject[] objects = new RNObject[]{contact};
		
		//BUILD AND SET PROCESSIN OPTION
		GetProcessingOptions options = new GetProcessingOptions();
		options.setFetchAllNames(false);
		
		try{
			ClientInfoHeader clientInfoHeader = new ClientInfoHeader();
			clientInfoHeader.setAppID("Basic Get");
			
			RNObjectsResult getResults = _service.get(objects, options, clientInfoHeader);
			RNObject[] rnObjects = getResults.getRNObjects();
			if(rnObjects != null){
				for(RNObject rnObject: rnObjects){
					contact = (Contact)rnObject;
					System.out.println("ID: "+ contact.getID().getId());
					System.out.println("First Name: "+ contact.getName().getFirst());
					System.out.println("Last Name: "+ contact.getName().getLast());
					
					EmailList e = contact.getEmails();
					if(e!=null){
						Email[] eList = e.getEmailList();
						if(eList != null){
							for(Email list: eList){
								System.out.println("Contact Email: "+list.getAddress());
							}
						}
					}
					isCustomFieldsSpecified = (contact.isCustomFieldsSpecified())? true:false;
					System.out.println("Is Custom Field Specified: " + isCustomFieldsSpecified);
					GenericField[] genericFields = contact.getCustomFields().getGenericFields();
					for(GenericField genericField : genericFields){
						GenericObject customField = genericField.getDataValue().getObjectValue();
						GenericField[] customFieldList = customField.getGenericFields();
						if(customFieldList != null){
							System.out.println("List not null");
							for(GenericField gf: customFieldList){
								//System.out.println(gf.getName() +"/" + gf.getDataType());
								
								DataValue dataValue = gf.getDataValue();
								if(dataValue !=null){
									
									if(dataValue.isNamedIDValueSpecified()){
										System.out.println(gf.getName()+":"+dataValue.getNamedIDValue().getID().getId());
										/*TEST CODE. DON'T UNCOMMENT
										if(gf.getName().compareTo("CountryOfCitizenship")==0){
											String countryName = countryLookup(dataValue.getNamedIDValue().getID().getId());
											System.out.println(countryName);
										}
										*/
									//}

/*									
									if(dataValue.isDateValueSpecified()){
										System.out.println(gf.getName()+":"+dataValue.getDateValue());
										
									}
									if(dataValue.isBooleanValueSpecified()){
										System.out.println(gf.getName()+":"+dataValue.getBooleanValue());
										
									}
								}
								else{
									System.out.println(gf.getName()+":");
								}
								
								
								/*
								GenericObject customFieldValue = gf.getDataValue().getObjectValue();
								GenericField customGF = customFieldValue.getGenericFields()[0];
								System.out.println(customGF.getDataValue().toString());
								*/
/*
							}
						}
						else{
							System.out.println("List is null");
						}
				}
/*
					
					
				}
			}
			else{
				System.out.println("Contact details not available");
				
			}
		}
		catch (RemoteException e)
        {
                System.out.println("Remote exception encountered: " + e.getMessage());
        }
        catch (ServerErrorFault e)
        {
                System.out.println("Server error exception encountered: " + e.getFaultMessage().getServerErrorFault().getExceptionMessage());
        }
        catch (RequestErrorFault e)
        {
                System.out.println("Request error exception encountered: " + e.getFaultMessage().getRequestErrorFault().getExceptionMessage());
        }
        catch (UnexpectedErrorFault e)
        {
                System.out.println("Unexpected error exception encountered:" + e.getFaultMessage().getUnexpectedErrorFault().getExceptionMessage());    
        }
		
		
	}
*/	

	/* TEST CODE ONLY
	private String countryLookup(long country_ID) throws RequestErrorFaultDetail{
		Country country = new Country();
		String countryName = null;
		ID countryID = new ID();
		countryID.setId(country_ID);
		country.setID(countryID);
		
		
		
	    //BUILD THE RN OBJECT ARRAY
		RNObject[] objects = new RNObject[]{country};
		
		//BUILD AND SET PROCESSIN OPTION
		GetProcessingOptions options = new GetProcessingOptions();
		options.setFetchAllNames(false);
		
		try{
			ClientInfoHeader clientInfoHeader = new ClientInfoHeader();
			clientInfoHeader.setAppID("Basic Get");
			
			RNObjectsResult getResults = _service.get(objects, options, clientInfoHeader);
			RNObject[] rnObjects = getResults.getRNObjects();
			System.out.println(rnObjects.length);
			if(rnObjects.length > 1){
				countryName = null;
				System.out.println("Multiple entries exists");
			}
			else{
				//System.out.println(rnObjects[0].getID());
				countryName = rnObjects[0].getLookupName();
			}
		
		    
		}
		catch (RemoteException e)
        {
                System.out.println("Remote exception encountered: " + e.getMessage());
        }
        catch (ServerErrorFault e)
        {
                System.out.println("Server error exception encountered: " + e.getFaultMessage().getServerErrorFault().getExceptionMessage());
        }
        catch (RequestErrorFault e)
        {
                System.out.println("Request error exception encountered: " + e.getFaultMessage().getRequestErrorFault().getExceptionMessage());
        }
        catch (UnexpectedErrorFault e)
        {
                System.out.println("Unexpected error exception encountered:" + e.getFaultMessage().getUnexpectedErrorFault().getExceptionMessage());    
        }
		return countryName;
	}*/
}
